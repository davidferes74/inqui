"use strict";

require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const cors = require("cors");

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());
app.use("/images", express.static(__dirname + "/images"));
app.use(express.json());

const pisosRouter = require("./routes/pisos-routes");
const bookingRouter = require("./routes/booking-routes");
const userRouter = require("./routes/users-routes");
const imagesRouter = require("./routes/images-routes");
const searchRouter = require("./routes/search-route");

app.use("/api/v1/pisos", pisosRouter);
app.use("/api/v1/booking", bookingRouter);
app.use("/api/v1/user", userRouter);
app.use("/api/v1/images", imagesRouter);
app.use("/api/v1/search", searchRouter);

const DEFAULT_PORT = 9999;

const currentPort = process.env.PORT || DEFAULT_PORT;

app.listen(currentPort, () => console.log(`Escuchando puerto: ${currentPort}`));
