const db = require("../../repositories/users-repository");

const getUserById = async (req, res) => {
  const { id } = req.params;

  try {
    const user = await db.getUserById(id);
    if (!user.length) {
      res.status(404).send();
    } else {
      res.send(user);
    }
  } catch (e) {
    res.status(500).send();
  }
};

module.exports = {
  getUserById,
};
