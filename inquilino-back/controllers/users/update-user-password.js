const db = require("../../repositories/users-repository");

const updateUserPassword = async (req, res) => {
  try {
    await passwordValidator.validateAsync(req.body);

    const { newPassword } = req.body;
    const { id } = req.params;

    const passwordBcrypt = await bcrypt.hash(newPassword, 10);
    await db.updateUserPassword(passwordBcrypt, id);
  } catch (e) {
    res.status(403).send();
  }
  res.send();
};

module.exports = {
  updateUserPassword,
};
