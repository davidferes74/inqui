const db = require("../../repositories/users-repository");

const bcrypt = require("bcrypt");

const { passwordValidator } = require("../../validators/users");
const resetPassword = async (req, res) => {
  try {
    await passwordValidator.validateAsync(req.body);

    const { code } = req.params;
    const { password } = req.body;

    const passwordBcrypt = await bcrypt.hash(password, 10);

    await db.resetPassword(passwordBcrypt, code);

    await db.checkUpdateCode(code);

    res.send("Su contraseña se ha actualizado");
  } catch (e) {
    res.status(403).send("Usuario no validado");
  }
  res.send();
};

module.exports = {
  resetPassword,
};
