const db = require("../../repositories/users-repository");

const randomstring = require("randomstring");

const utils = require("../../utils/utils");

const { emailValidator } = require("../../validators/users");

const recoverPassword = async (req, res) => {
  try {
    await emailValidator.validateAsync(req.body);

    const { email } = req.body;
    console.log(email);

    const user = await db.getUser(email);

    if (!user) {
      res.status(401).send();
      return;
    }
    const validationCode = randomstring.generate(21);

    await db.updatePasswordCode(validationCode, email);

    utils.recoverPasswordMail(
      email,
      `http://${process.env.FRONT_DOMAIN}/reset/${validationCode}`
    );
  } catch (e) {
    console.log(e);
    res.status(403).send();
  }
  res.send();
};

module.exports = {
  recoverPassword,
};
