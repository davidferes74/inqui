const db = require("../../repositories/users-repository");

const randomstring = require("randomstring");

const updateUserEmail = async (req, res) => {
  try {
    await emailValidator.validateAsync(req.body);

    const { email } = req.body;
    const { id } = req.params;

    const validationCode = randomstring.generate(21);

    await db.updateUserEmail(email, id, validationCode);

    utils.updateEmailMail(
      email,
      `http://${process.env.FRONT_DOMAIN}/validate/${validationCode}`
    );
  } catch (e) {
    console.log(e);
    res.status(403).send();
  }
  res.send();
};

module.exports = {
  updateUserEmail,
};
