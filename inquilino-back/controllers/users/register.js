const bcrypt = require("bcrypt");
const randomstring = require("randomstring");

const db = require("../../repositories/users-repository");
const utils = require("../../utils/utils");

const register = async (req, res) => {
  try {
    const {
      email,
      password,
      nombre,
      provincia,
      apellidos,
      ciudad,
      descripcion,
      fechaNacimiento,
    } = req.body;
    const user = await db.getUser(email);
    if (user) {
      res.status(400).send();
      return;
    }
    const passwordBcrypt = await bcrypt.hash(password, 10);

    const validationCode = randomstring.generate(21);

    await db.register(
      email,
      passwordBcrypt,
      nombre,
      provincia,
      apellidos,
      ciudad,
      descripcion,
      fechaNacimiento,
      validationCode
    );

    utils.sendConfirmationMail(
      email,
      `http://${process.env.FRONT_DOMAIN}/validate/${validationCode}`
    );
  } catch (e) {
    console.log(e);
    res.status(400).send();
    return;
  }

  res.send();
};

module.exports = {
  register,
};
