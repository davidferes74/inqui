const db = require("../../repositories/bookings-repository");

const getBooking = async (req, res) => {
  const { id } = req.params;

  try {
    const booking = await db.getBooking(id);

    if (!booking) {
      console.log("Es este ");
      res.status(404).send();
    } else {
      res.send(booking);
    }
  } catch (e) {
    res.status(500).send();
  }
};
module.exports = {
  getBooking,
};
