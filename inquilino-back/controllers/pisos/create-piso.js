const dbpiso = require("../../repositories/pisos-repository");
const dbimage = require("../../repositories/pisos-repository");
const jwt = require("jsonwebtoken");
const fsPromises = require("fs").promises;
const uuid = require("uuid");

const addPiso = async (req, res) => {
  const {
    provincia,
    ciudad,
    direccion,
    precio_piso,
    nBanos,
    nHabitaciones,
    m2,
    descripcion,
    id_usuario,
    fechaPublicacion,
  } = req.body;

  const { authorization } = req.headers;

  try {
    const decodedToken = jwt.verify(authorization, process.env.SECRET);
    const id_usuario = decodedToken.id;

    const data = await dbpiso.createHome(
      fechaPublicacion,
      provincia,
      ciudad,
      direccion,
      precio_piso,
      nBanos,
      nHabitaciones,
      m2,
      descripcion,
      id_usuario
    );

    res.send(data);

    console.log(data);

    if (req.files) {
      await fsPromises.mkdir(process.env.TARGET_FOLDER, { recursive: true });

      try {
        const fileID = uuid.v4();
        const outputFileName = `${process.env.TARGET_FOLDER}/${fileID}.jpg`;

        await fsPromises.writeFile(outputFileName, req.files.imagen.data);

        await dbimage.saveHomeImage(fileID, data.insertId);

        res.send();
      } catch (e) {
        console.log("Error: ", e);
        res.status(500).send();
      }
    }
  } catch (e) {
    console.log(e);
    let statusCode = 400;
    if (e.message === "database-error") {
      statusCode = 500;
    }
    res.status(statusCode).send(e.message);
    return;
  }

  res.send();
};

module.exports = {
  addPiso,
};
