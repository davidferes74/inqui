const db = require("../../repositories/pisos-repository");

const getPiso = async (req, res) => {
  const { id } = req.params;
  console.log("holaaa");
  try {
    const home = await db.getHome(id);
    console.log(home);
    if (!home.length) {
      res.status(404).send();
    } else {
      res.send(home);
    }
  } catch (e) {
    res.status(500).send();
  }
};

module.exports = {
  getPiso,
};
