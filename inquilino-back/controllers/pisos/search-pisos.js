const db = require("../../repositories/pisos-repository");

const searchPisos = async (req, res) => {
  
  try {
    console.log('hola');
      const pisos = await db.filterPisos(req.query);
      res.send(pisos);
  } catch (e) {
    res.status(500).send();
  }
};

module.exports = {
  searchPisos,
};
