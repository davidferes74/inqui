const db = require("../../repositories/images-repositories");

const fsPromises = require("fs").promises;
const uuid = require("uuid");

const saveHomeImage = async (req, res) => {
  const { id } = req.params;

  await fsPromises.mkdir(process.env.TARGET_FOLDER, { recursive: true });

  try {
    const fileID = uuid.v4();
    const outputFileName = `${process.env.TARGET_FOLDER}/${fileID}.jpg`;

    await fsPromises.writeFile(outputFileName, req.files.imagen.data);

    await db.saveHomeImage(fileID, id);

    res.send();
  } catch (e) {
    console.log("Error: ", e);
    res.status(500).send();
  }
};

module.exports = {
  saveHomeImage,
};
