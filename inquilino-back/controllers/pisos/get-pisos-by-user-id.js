const db = require("../../repositories/pisos-repository");

const getMyHomes = async (req, res) => {
  const { id } = req.params;

  try {
    const myHomes = await db.myHomes(id);
    if (!myHomes.length) {
      res.status(404).send();
    } else {
      res.send(myHomes);
    }
  } catch (e) {
    res.status(500).send();
  }
};
module.exports = {
  getMyHomes,
};
