const dbpiso = require("../../repositories/pisos-repository");
const dbimage = require("../../repositories/images-repository");

const fsPromises = require("fs").promises;
const uuid = require("uuid");

const updatePiso = async (req, res) => {
  const {
    provincia,
    ciudad,
    direccion,
    precio_piso,
    nBanos,
    nHabitaciones,
    m2,
    descripcion,
    id_usuario,
  } = req.body;

  const { id } = req.params;

  if (req.files) {
    await fsPromises.mkdir(process.env.TARGET_FOLDER, { recursive: true });

    try {
      const fileID = uuid.v4();
      const outputFileName = `${process.env.TARGET_FOLDER}/${fileID}.jpg`;

      await fsPromises.writeFile(outputFileName, req.files.imagen.data);

      await dbimage.saveHomeImage(fileID, id);

      res.send();
    } catch (e) {
      console.log("Error: ", e);
      res.status(500).send();
    }
  }

  try {
    // await homeValidator.validateAsync(req.body)
    await dbpiso.updatePiso(
      provincia,
      ciudad,
      direccion,
      precio_piso,
      nBanos,
      nHabitaciones,
      m2,
      descripcion,
      id_usuario,
      id
    );
  } catch (e) {
    console.log(e);
    let statusCode = 400;
    if (e.message === "database-error") {
      statusCode = 500;
    }
    res.status(statusCode).send(e.message);
    return;
  }
  res.send();
};

module.exports = {
  updatePiso,
};
