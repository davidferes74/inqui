"use strict";

const { getConnection } = require("../db/db");
const performQuery = async (query, params) => {
  let connection;
  console.log(query);
  console.log(params);

  try {
    connection = await getConnection();

    const [result] = await connection.query(query, params);

    return result;
  } catch (e) {
    console.log(e);
    throw new Error("database-error");
  } finally {
    if (connection) {
      connection.release();
    }
  }
};

const register = async (
  email,
  password,
  nombre,
  provincia,
  apellidos,
  ciudad,
  descripcion,
  fechaNacimiento,
  validationCode
) => {
  const query = `insert into usuario (email, password, nombre, provincia, apellidos, ciudad, descripcion, fechaNacimiento, validationCode) values (?,?,?,?,?,?,?,?,?)`;
  const params = [
    email,
    password,
    nombre,
    provincia,
    apellidos,
    ciudad,
    descripcion,
    fechaNacimiento,
    validationCode,
  ];
  console.log(params);

  await performQuery(query, params);
};

const getUser = async (email) => {
  const query = `select * from usuario where email = ?`;
  const params = [email];

  const [result] = await performQuery(query, params);
  return result;
};

const getUserById = async (id) => {
  const query = `select u.apellidos,
                    u.ciudad,
                    u.descripcion,
                    u.email,
                    u.estado,
                    u.id,
                    u.imagen,
                    u.nombre,
                    u.provincia,
                    u.role,
                    avg(r.score_usuario) as score_usuario,
                    count(r.score_usuario) as count_score_usuario,
                    validationCode
                    from usuario u left join reserva r on r.id_usuario = u.id where u.id = ? group by u.id`;
  const params = [id];

  const result = await performQuery(query, params);
  return result;
};

const updateUser = async (
  nombre,
  apellidos,
  fechaNacimiento,
  provincia,
  ciudad,
  descripcion,
  telefono,
  id
) => {
  const query = `
    update usuario set nombre = ?,
    apellidos = ?,
    fechaNacimiento = ?,
    provincia = ?,
    ciudad = ?,
    descripcion = ?,
    telefono = ?
    where id = ?
    `;
  const params = [
    nombre,
    apellidos,
    fechaNacimiento,
    provincia,
    ciudad,
    descripcion,
    telefono,
    id,
  ];
  console.log(params);

  await performQuery(query, params);
};

const updateUserPassword = async (password, id) => {
  const query = `update usuario set password = ?
        where id = ?`;
  const params = [password, id];

  await performQuery(query, params);
};

const updateUserEmail = async (email, id, validationCode) => {
  const query = `update usuario set email = ?, validationCode = ? where id = ?`;
  const params = [email, validationCode, id];

  await performQuery(query, params);
};

const updatePasswordCode = async (validationCode, email) => {
  const query = `update usuario set validationCode = ? where email = ?`;
  const params = [validationCode, email];

  await performQuery(query, params);
};

const resetPassword = async (password, validationCode) => {
  const query = `update usuario set password = ? where validationCode = ?`;
  const params = [password, validationCode];

  await performQuery(query, params);
};

const deleteUserById = async (id) => {
  const query = `delete from usuario where id = ?`;
  const params = [id];

  await performQuery(query, params);
};

const checkValidationCode = async (code) => {
  const query = `select * from usuario where validationCode = ?`;
  const params = [code];

  const result = await performQuery(query, params);
  console.log(result);

  if (result) {
    const query = `update usuario set estado = 'activo', validationCode = '' where validationCode = ?`;
    await performQuery(query, [code]);
  } else {
    throw new Error("validation-error");
  }
};

const checkUpdateCode = async (code) => {
  const query = `select * from usuario where validationCode = ?`;
  const params = [code];

  const [result] = await performQuery(query, params);

  try {
    if (result) {
      const query = `update usuario set estado = 'activo', validationCode = '' where validationCode = ?`;
      await performQuery(query, [code]);
    }
  } catch (e) {
    console.log(e);
  }
};

module.exports = {
  register,
  getUser,
  getUserById,
  updateUser,
  updateUserPassword,
  updateUserEmail,
  updatePasswordCode,
  resetPassword,
  updateUser,
  deleteUserById,
  checkValidationCode,
  checkUpdateCode,
};
